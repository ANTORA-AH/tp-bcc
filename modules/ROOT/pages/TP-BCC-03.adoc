= Cours 03 - Evaluation / Correction
:author: Arnaud Harbonnier
:email: arnaud.harbonnier@valarep.fr
:title-page:
//firstname middlename lastname <email>firstname middlename lastname <email>; firstname middlename lastname <email>
//Arnaud Harbonnier <arnaud.harbonnier@gmail.com>
//Arnaud Harbonnier <arnaud.harbonnier@valarep.fr>
//Arnaud Harbonnier <arnaud.harbonnier@ltpdampierre.fr>
//doctype: book
//:title-logo-image: image:logo-judo-saintsaulve.jpg[pdfwidth=3.5in,align=center]
:sectnums:
:toc:
:toc-title:
:toclevels: 4
:source-highlighter: highlightjs
//icons: font
:imagesdir: ./images/TP-BCC-03
:sectanchors: 
//image::sunset.jpg[Sunset,300,400]
//image::sunset.jpg[alt=Sunset,width=300,height=400]
//:revnumber: 29/09/2023
:revdate: VALAREP | TP-BCC
//:revremark: 1
//:version-label!: 
//:version-label: Cours n° 
:experimental:
:icons: font


1)	Définissez INTERNET / donnez plusieurs applications.
	
[red]#INTERNET est trop souvent restreint à la consultation de sites web (www= World Wide Web).# +
[red]#INTERNET est appelé _le Réseau des Réseaux_, il constitue avant tout un support mondial de communication.# +
[red]#Ses applications :# +
[red]#- Recherche sur le net (www)# +
[red]#- Messagerie électronique (mails)# +
[red]#- Messagerie instantanée (chat)# +
[red]#- Transfert de fichiers (FTP)# +
[red]#- Vidéo# +
[red]#- ...#
	
2)	Comment nomme -ton (Acronyme – 3 lettres) le segment réseau qui se trouve derrière la box (côté habitation) ? Donnez a signification de chaque lettre.
	
[red]#LAN# +
[red]#- Local# +
[red]#- Area# +
[red]#- Network#
	
3)	Même question pour le segment qui se trouve de l’autre côté (côté internet) ? Donnez la signification de chaque lettre.
	
[red]#WAN# +
[red]#- Wide# +
[red]#- Area# +
[red]#- Network#
	
4)	Comment s’appelle le service qui permet d’envoyer une configuration IP aux hôtes du réseau  ?
	
[red]#Le service DHCP# +
[red]#Dynamic Host Configuration Protocol# +
[red]#Il est implémenté au niveau de la box ou d'un serveur.#
	
5)	Comment s’appelle le service qui permet de résoudre les noms de domaine en adresse IP ? Ex : valarep.fr / 213.186.33.4
	
[red]#Le service DNS# +
[red]#Domain Name System#

	
6)	Qu’est-ce qu’un octet ?

[red]#Un octet représente 8 bits# +
[red]#Un bit est un chiffre binaire, il peut prendre la valeur 0 ou 1#

[WARNING]
[red]#_Octet_ se dit _BYTE_ en anglais !!!#
	
7)	Quelle est la plus petite valeur que peut prendre un octet ? pourquoi ?

[red]#La plus petite valeur d'un octet est 0 car les 8 bits sont à 0#
	
	
8)	Quelle est la plus grande valeur que peut prendre un octet ? pourquoi ?

[red]#La plus grande valeur d'un octet est 255 car les 8 bits sont à 1.# +
[red]#2^7^+2^6^+2^5^+2^4^+2^3^+2^2^+2^1^+2^0^ = 255# +
[red]#128+64+32+16+8+4+2+1 = 255#
	
9)	Qu’est-ce qu’une adresse IP V4 ? de combien de bits est-elle constituée ?
	
[red]#C'est une adresse logique - elle peut changer# +
[red]#Elle est codée sur 4 octets soit 32 bits pour IP V4# +
[red]#Elle est codée sur 16 octets soit 128 bits pour IP V6# +

10)	Comment appelle-t-on l'adresse physique ou matérielle d’un hôte réseau ? A quel composant est-elle attachée ?
	
[red]#Adresse MAC (Media Access Control). Elle est liée à la carte reseau et ne peut en principe pas être modifiée.#
[red]#Quelques utilisations :# +
[red]#- Filtrage par adresse MAC au niveau des box internet. Cela permet de déclarer les équipements connus comme étant de confiance et d'éviter que des hôtes inconnus s'invitent sur le réseau# +
[red]#- Création de réservations au niveau du service DHCP. Cela permet que le service DHCP envoie toujours la même adresse IP à un équipement. Ceci est possible grâce un identifiant  de la machine qui ne varie pas (attaché à sa carte réseau) ; l'adresse MAC.#
[red]#- sécurisation des switches (commutateurs). Un équipement dont l'adresse MAC n'a pas été approuvé ne pourra pas avoir de connectivité# +
[red]#- L'usurpation d'adresse MAC (ARP poisoning = permet de se faire passer pour une autre machine) permet de mettre en place une attaque dite _de l'homme du milieu_ (MITM comme _Man In The Middle_)# +

11)	Dans quel système de numération est codé une adresse MAC ?

[red]#L'Hexadécimal, base 16 (0 à F)#
	
12)	Qu’est-ce qu’un protocole de communication ?
	
[red]#Ensemble de normes et de règles qui régissent les communications entre un hôte client et un hôte serveur. Dit de façon plus triviale, un protocole sert à mettre tout en oeuvre pour que le client et le serveur parlent la même langue pour se comprendre et fonctionner ensemble#
	
13)	A quoi sert le protocole FTP:// ?

[red]#Procole dédié au transfert de fichiers#
	
14)	A quoi sert le protocole http:// ? citez 3 clients web.

[red]#Protocole dédié au web - numéro de port http://IP:80 et https://IP:443# +
[red]#Clients web :# +
[red]#- Mozilla Firefox# +
[red]#- Safari# +
[red]#- Google Chrome# +

15)	Qu’est-ce qu’un connecteur RJ45 ?

[red]#Connecteur pour câble éthernet.#

image:ROOT:TP-BCC-03/2023-09-01T14-56-47-879Z.png[]

image:ROOT:TP-BCC-03/2023-09-01T14-58-06-401Z.png[] 
	
	
16)	Donnez la valeur décimale de l’octet suivant : 11001101 – Laissez vos calculs apparents.

|===
|2^7^|2^6^|2^5^|2^4^|2^3^|2^2^|2^1^|2^0^
|1|1|0|0|1|1|0|1
|128|64|0|0|8|4|0|1
	
|===

[red]#128+64+8+4+1=205#
	

17)	Donnez la valeur binaire de 168 – Laissez vos calculs apparents .

|===
|2^7^|2^6^|2^5^|2^4^|2^3^|2^2^|2^1^|2^0^
|128|64|32|16|8|4|2|1
|1|0|1|0|1|0|0|0	
|===

[red]#168=128+32+8#
	
	
18)	Qu’est ce que le CPL ?

[red]#Courant Porteur en Ligne (Powerline en anglais).# +
[red]#L'information transite / est véhiculée via le réseau électrique.#

	
19)	Que veut dire IOT (donnez la signification de chaque lettre)

[red]#Internet Of Things = Internet des Objets#	
	
20)	Que veut dire WLAN (donnez la signification de chaque lettre)

[red]#Wireless Local Area Network# +
[red]#Le préfixe _-less_ en anglais veut dire _sans_.# +
[red]#WLAN = Réseaux sans fils#

[NOTE]
Ce qui suit rappelle les notions évoquées durant la correction. Nous y reviendrons.

== les modèles informatique

Ils servent à décrire les communications sur un réseau. Ils sont aussi une excellente base pour la méthodologie de résolution de pannes.

image:ROOT:TP-BCC-03/2023-08-21T09-40-57-607Z.png[] 

=== Mododèle OSI

image:ROOT:TP-BCC-03/2023-08-21T09-38-41-329Z.png[] 


=== Modèle TCP/IP

image:ROOT:TP-BCC-03/2023-08-21T09-39-07-078Z.png[]


|===
|*N° de couche*|*Nom*|*Mots-clés*| *Equipements*
|7|/||
|6|/||
|5|/||
|4|Transport| Ports Source et Destination (UDP - TCP)| Firewall (ouvrir / fermer des ports)
|3|Réseau| Adresses IP Source & Destination| Routeur
|2|Liaison de données| Adresses MAC Source et Destination | Switch / Commutateur
|1|Physique| Connectique, interfaces (RJ45...) | Hub / Concentrateur
|===




